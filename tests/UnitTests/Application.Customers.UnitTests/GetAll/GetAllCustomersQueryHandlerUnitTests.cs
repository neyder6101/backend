﻿using Application.Customers.GetAll;
using Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Customers.UnitTests.GetAll
{
    public class GetAllCustomersQueryHandlerUnitTests
    {

        private readonly Mock<ICustomerRepository> _mockCustomerRepository;

        private readonly GetAllCustomersQueryHandler _handler;

        public GetAllCustomersQueryHandlerUnitTests()
        {
            _mockCustomerRepository = new Mock<ICustomerRepository>();
            _handler = new GetAllCustomersQueryHandler(_mockCustomerRepository.Object);
        }

        [Fact]
        public async Task HandlerGetAllCustomers_When_NotResult()
        {

            var result = _handler.Handle(null, default);
            Assert.NotNull(result);
        }

    }
}
