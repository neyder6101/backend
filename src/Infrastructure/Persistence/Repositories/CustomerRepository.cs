using Domain.Customers;
using Domain.ValueObjects;

namespace Infrastructure.Persistence.Repositories;

public class CustomerRepository : ICustomerRepository
{
    public List<Customer> _customers;

    public CustomerRepository()
    {
        _customers = new List<Customer>
        {
            new Customer { Id = new CustomerId(new Guid()),
                Name = "Neyder",
                LastName = "Angarita",
                Email = "neyder6101@gmail.com",
                Active = true,
                PhoneNumber = new PhoneNumber("66666666"),
                Address = new Address("Colombia", "Calle", "34 # 34c 47", "Villavicencio", "Meta", "50000001")                
            },
            new Customer { Id = new CustomerId(new Guid()),
                Name = "Camilo",
                LastName = "Andres",
                Email = "camilo.andres@gmail.com",
                Active = true,
                PhoneNumber = new PhoneNumber("66778765455"),
                Address = new Address("Colombia", "Calle", "32 # 16c 47", "Bogot�", "Bogot� D.C", "50000001")
            },
        };
    }

    public async Task<List<Customer>> GetAll()
    {
        await Task.Delay(1); // Simular operaci�n asincr�nica
        return _customers;
    }
}