using Domain.Primitives;

namespace Domain.Customers;

public sealed class Customer : AggregateRoot
{
    public Customer(CustomerId id, string name, string lastName, string email, 
    PhoneNumber phoneNumber, Address address, bool active)
    {
        Id = id;
        Name = name;
        LastName = lastName;
        Email = email;
        PhoneNumber = phoneNumber;
        Address = address;
        Active = active;
    }

    public Customer()
    {

    }

    public CustomerId Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string FullName => $"{Name} {LastName}";
    public string Email { get; set; } = string.Empty;
    public PhoneNumber PhoneNumber { get; set; }
    public Address Address { get; set; }
    public bool Active { get; set; }

    public static Customer UpdateCustomer(Guid id, string name, string lastName, string email, PhoneNumber phoneNumber, Address address, bool active)
    {
        return new Customer(new CustomerId(id), name, lastName, email, phoneNumber, address, active);
    }
}