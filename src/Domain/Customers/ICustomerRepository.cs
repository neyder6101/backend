namespace Domain.Customers;

public interface ICustomerRepository
{
    Task<List<Customer>> GetAll();
}